/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.webservices;

//import com.cedarsoftware.util.io.JsonReader;
//import com.cedarsoftware.util.io.JsonWriter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import indi.roadworkEmergency.util.RENSRoadworkRequest;
import static java.lang.System.in;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import org.json.JSONArray;
import org.json.JSONObject;
import indi.roadworkEmergency.share.*;
import indi.roadworkEmergency.util.RENSDBControl;
import indi.roadworkEmergency.util.RENSJSONParser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import static java.security.AccessController.getContext;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.annotation.Resource;
import javax.json.JsonWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import oracle.spatial.geometry.JGeometry;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.core.MediaType;
import org.json.JSONException;


/**
 * REST Web Service
 *
 * @author syunwei
 */
@Path("roadworks")
public class RoadworkImpl {

    private RENSDBControl dbControl = new RENSDBControl();
    
    @Context
    private UriInfo context;
    /**
     * Creates a new instance of Roadwork
     */
    public RoadworkImpl() {
    }
    
    
    /**
     * Retrieves representation of an instance of indi.roadworkEmergency.webservices.RoadworkImpl
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getRoadworks() throws Exception {
        
        //response.put(SOAJSONTags.QUANTITY, quantity);
        RENSRoadworkRequest trafficEventRequest = new RENSRoadworkRequest();
        String responseStr = trafficEventRequest.request();
        
        //String mockStr = getFile("/test/roadworksData.txt");
        //File file = request.getServletContext().getRealPath("/files/BB.key");
        

        
        
        Map<String,List<Roadwork>> roadworksMap = new HashMap<String,List<Roadwork>>();
        
        List<Roadwork> roadworks = dbControl.selectAllRoadworks();
        roadworksMap.put("roadworks",roadworks);
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();//new Gson(); //
        
        String returnJSON = gson.toJson(roadworksMap);
        returnJSON = returnJSON.replaceAll("(\\\\r\\\\n|\\\\n|\\\\r)", "\\n");
        returnJSON = returnJSON.replace("Nan", "null");
        return returnJSON;
    }

    /**
     * POST method for updating or creating an instance of RoadworkImpl
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String getRoadworks(String content) throws JSONException, Exception {
        
        double longitude = 0.f;
        double latitude = 0.f;
        double radius = 1.f;
        
        JSONObject jsonObject = new JSONObject(content);
        if(jsonObject.has("longitude"))
        {
            longitude = jsonObject.getDouble("longitude");
        }
        
        if(jsonObject.has("latitude"))
        {
            latitude = jsonObject.getDouble("latitude");
        }
        
        if(jsonObject.has("radius"))
        {
            radius = jsonObject.getDouble("radius");
        }
        
        Map<String,List<Roadwork>> roadworksMap = new HashMap<String,List<Roadwork>>();
        List<Roadwork> roadworks = dbControl.selectNearestRoadworks(longitude, latitude, radius);
        roadworksMap.put("roadworks",roadworks);
        Gson gson = new GsonBuilder().serializeSpecialFloatingPointValues().create();
        String returnJSON = gson.toJson(roadworksMap);
        returnJSON = returnJSON.replaceAll("(\\\\r\\\\n|\\\\n|\\\\r)", "\\n");
        returnJSON = returnJSON.replace("NaN", "null");
        return returnJSON;
    }
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public void putRoadwork(String content) throws JSONException, ParseException, Exception
    {
        List<Roadwork> roadworks = RENSJSONParser.parseRoadwork(content);
        for(Roadwork roadworkObj : roadworks)
        {
            dbControl.insertRoadwork(roadworkObj);
        }
    }
    
    @DELETE
    @Path("/{roadworkId}")
    public void deleteRoadwork(@PathParam("roadworkId") long roadworkId) throws SQLException
    {
       dbControl.deleteRoadwork(roadworkId);
    }
}
