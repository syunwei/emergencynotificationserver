/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.webservices;

import indi.roadworkEmergency.util.RENSDBControl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author pc1
 */
@Path("userlocation")
public class UserLocationImpl {

    RENSDBControl dbControl;
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of UserLocation
     */
    public UserLocationImpl() {
        dbControl = new RENSDBControl();
    }

    /**
     * Retrieves representation of an instance of indi.roadworkEmergency.webservices.UserLocationImpl
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getUserLocation() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of UserLocationImpl
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes("application/json")
    public void postUserLocation(String content) throws JSONException, Exception 
    {
        double longitude = 0.f;
        double latitude = 0.f;
        long userid = 0;
        
        JSONObject jsonObject = new JSONObject(content);
        if(jsonObject.has("longitude"))
        {
            longitude = jsonObject.getDouble("longitude");
        }
        
        if(jsonObject.has("latitude"))
        {
            latitude = jsonObject.getDouble("latitude");
        }
        
        if(jsonObject.has("userid"))
        {
            userid = jsonObject.getLong("userid");
        }
        
        dbControl.updateUserLocation(longitude,latitude,userid);
    }
}
