/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.webservices;

import com.google.gson.Gson;
import indi.roadworkEmergency.share.Roadwork;
import indi.roadworkEmergency.share.User;
import indi.roadworkEmergency.util.RENSDBControl;
import java.sql.SQLException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author pc1
 */
@Path("token")
public class PushNotificationTokenImpl {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RegistPushTokenImpl
     */
    public PushNotificationTokenImpl() {
    }

    /**
     * Retrieves representation of an instance of indi.roadworkEmergency.webservices.PushNotificationTokenImpl
     * @param deviceid
     * @return an instance of java.lang.String
     * @throws java.sql.SQLException
     * @throws org.json.JSONException
     */
    @GET
    @Path("/{deviceid}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getToken(@PathParam("deviceid") String deviceid) throws SQLException, JSONException {
        JSONObject jsonObject = new JSONObject();
        RENSDBControl dbControl = new RENSDBControl();
        String token = dbControl.getUserToken(deviceid);
        jsonObject.put("deviceid",deviceid);
        jsonObject.put("token", token);
        
        return jsonObject.toString();
    }

    /**
     * PUT method for updating or creating an instance of PushNotificationTokenImpl
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void postToken(String content) throws SQLException, JSONException
    {
        JSONObject jsonObject = new JSONObject(content);
        User user = new User();
        Object deviceid = jsonObject.get("deviceid");
        if(deviceid != JSONObject.NULL)
        {
            user.setDeviceId(deviceid.toString());
        }
        
        Object token = jsonObject.get("token");
        if(token != JSONObject.NULL)
        {
            user.setToken(token.toString());
        }
        
        Object userid = jsonObject.getLong("userid");
        if(userid != JSONObject.NULL)
        {
            user.setUserId(jsonObject.getLong("userid"));
        }
        
        Object username = jsonObject.get("username");
        if(username != JSONObject.NULL)
        {
            user.setUserName(username.toString());
        }
        RENSDBControl dbControl = new RENSDBControl();
        dbControl.insertUserToken(user);
    }
}
