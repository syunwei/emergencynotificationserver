/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.webservices;

import indi.roadworkEmergency.util.RENSPushControl;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author pc1
 */
@Path("push")
public class PushNotificationImpl {

    @Context
    private UriInfo context;
    private RENSPushControl pushControl;

    /**
     * Creates a new instance of PushNotificationImpl
     */
    public PushNotificationImpl() {
         pushControl = new RENSPushControl();
    }

    /**
     * Retrieves representation of an instance of indi.roadworkEmergency.webservices.PushNotificationImpl
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * POST method for updating or creating an instance of PushNotificationImpl
     * @param content representation for the resource
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void pushNotification(String content) throws JSONException, CommunicationException, KeystoreException, SQLException {
        JSONObject jsonObject = new JSONObject(content);
        JSONArray deviceids = null;
        if(jsonObject.has("deviceids"))
        {
            deviceids = jsonObject.getJSONArray("deviceids");
        }
        Object pushContent = jsonObject.get("content");
        
        List<String> deviceisList = new ArrayList<String>();
        
        if(pushContent != JSONObject.NULL)
        {
            if( deviceids != null)
            {
                int len = deviceids.length();
                for (int i=0;i<len;i++)
                { 
                    deviceisList.add(deviceids.get(i).toString());
                } 
            
                RENSPushControl.pushToDevices(deviceisList, content);
            }
            else
            {
                pushControl.pushToAll(pushContent.toString());
            }
            
        }
    }
    
  
}
