/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

/**
 *
 * @author pc1
 */
public class SDOGeometry {
    
    public String name;

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    public int population;

    /**
     * Get the value of population
     *
     * @return the value of population
     */
    public int getPopulation() {
        return population;
    }

    /**
     * Set the value of population
     *
     * @param population new value of population
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    public double lontitude = 0.f;

    /**
     * Get the value of lontitude
     *
     * @return the value of lontitude
     */
    public double getLontitude() {
        return lontitude;
    }

    /**
     * Set the value of lontitude
     *
     * @param lontitude new value of lontitude
     */
    public void setLontitude(double lontitude) {
        this.lontitude = lontitude;
    }

    public double latitude = 0.f;

    /**
     * Get the value of latitude
     *
     * @return the value of latitude
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Set the value of latitude
     *
     * @param latitude new value of latitude
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

}
