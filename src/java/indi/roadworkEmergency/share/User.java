/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

import java.util.List;

/**
 *
 * @author pc1
 */
public class User {
    
    private static User instance = null;
    private long userId = 0;
    private String userName = "wei";
    private String deviceId = "1_1";
    private String token;

    

    
    public static synchronized User shareUser() {
      if(instance == null) {
         instance = new User();
      }
      return instance;
   }
    
    /**
     * Get the value of userId
     *
     * @return the value of userId
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Set the value of userId
     *
     * @param userId new value of userId
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    

    /**
     * Get the value of userName
     *
     * @return the value of userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Set the value of userName
     *
     * @param userName new value of userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    

    /**
     * Get the value of device id
     *
     * @return the value of device id*/
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Set the value of device id
     *
     * @param deviceId new value of device id
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Get the value of token
     *
     * @return the value of token
     */
    public String getToken() {
        return token;
    }

    /**
     * Set the value of token
     *
     * @param token new value of token
     */
    public void setToken(String token) {
        this.token = token;
    }
}
