/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

import java.util.Date;

/**
 *
 * @author pc1
 */
public class Temporal {
    
    public long temporalId;
    private long roadworkId;
    /**
     * Get the value of roadworkId
     *
     * @return the value of roadworkId
     */
    public long getRoadworkId() {
        return roadworkId;
    }

    /**
     * Set the value of roadworkId
     *
     * @param roadwork_id new value of roadworkId
     */
    public void setRoadworkId(long roadwork_id) {
        this.roadworkId = roadwork_id;
    }
    
    
    /**
     * Get the value of temporalId
     *
     * @return the value of temporalId
     */
    public long getTemporalId() {
        return temporalId;
    }

    /**
     * Set the value of temporalId
     *
     * @param temporalId new value of temporalId
     */
    public void setTemporalId(long temporalId) {
        this.temporalId = temporalId;
    }

    

    private Date startDate;

    /**
     * Get the value of startDate
     *
     * @return the value of startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Set the value of startDate
     *
     * @param startDate new value of startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date modifiedDate;

    /**
     * Get the value of modifiedDate
     *
     * @return the value of modifiedDate
     */
    public Date getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Set the value of modifiedDate
     *
     * @param modifiedDate new value of modifiedDate
     */
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date endDate;

    /**
     * Get the value of endDate
     *
     * @return the value of endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Set the value of endDate
     *
     * @param endDate new value of endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date review;

    /**
     * Get the value of review
     *
     * @return the value of review
     */
    public Date getReview() {
        return review;
    }

    /**
     * Set the value of review
     *
     * @param review new value of review
     */
    public void setReview(Date review) {
        this.review = review;
    }

    public Date lastReview;

    /**
     * Get the value of lastReview
     *
     * @return the value of lastReview
     */
    public Date getLastReview() {
        return lastReview;
    }

    /**
     * Set the value of lastReview
     *
     * @param lastReview new value of lastReview
     */
    public void setLastReview(Date lastReview) {
        this.lastReview = lastReview;
    }

    public String nextUpdate;

    /**
     * Get the value of nextUpdate
     *
     * @return the value of nextUpdate
     */
    public String getNextUpdate() {
        return nextUpdate;
    }

    /**
     * Set the value of nextUpdate
     *
     * @param nextUpdate new value of nextUpdate
     */
    public void setNextUpdate(String nextUpdate) {
        this.nextUpdate = nextUpdate;
    }

}
