/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

import oracle.spatial.geometry.JGeometry;

/**
 *
 * @author pc1
 */
public class Geometry {
    private long geometryId;
    private String type;
    private String address;
    private long roadworkId;
    public JGeometry location;
    
    public Geometry()
    {
        
    }
    
    

    /**
     * Get the value of geometryId
     *
     * @return the value of geometryId
     */
    public long getGeometryId() {
        return geometryId;
    }

    /**
     * Set the value of geometryId
     *
     * @param geometryId new value of geometryId
     */
    public void setGeometryId(long geometryId) {
        this.geometryId = geometryId;
    }


    

    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Set the value of type
     *
     * @param type new value of type
     */
    public void setType(String type) {
        this.type = type;
    }

    

    /**
     * Get the value of address
     *
     * @return the value of address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Set the value of address
     *
     * @param address new value of address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    

    /**
     * Get the value of location
     *
     * @return the value of location
     */
    public JGeometry getLocation() {
        return location;
    }

    /**
     * Set the value of location
     *
     * @param location new value of location
     */
    public void setLocation(JGeometry location) {
        this.location = location;
    }

    

    /**
     * Get the value of roadworkId
     *
     * @return the value of roadworkId
     */
    public long getRoadworkId() {
        return roadworkId;
    }

    /**
     * Set the value of roadworkId
     *
     * @param roadworkId new value of roadworkId
     */
    public void setRoadworkId(long roadworkId) {
        this.roadworkId = roadworkId;
    }

}
