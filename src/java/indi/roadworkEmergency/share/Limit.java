/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

/**
 *
 * @author pc1
 */
public class Limit {
    
    public long limitId;
    private long roadworkId;
    
    
    /**
     * Get the value of roadworkId
     *
     * @return the value of roadworkId
     */
    public long getRoadworkId() {
        return roadworkId;
    }

    /**
     * Set the value of roadworkId
     *
     * @param roadworkId new value of roadworkId
     */
    public void setRoadworkId(long roadworkId) {
        this.roadworkId = roadworkId;
    }
    /**
     * Get the value of limitId
     *
     * @return the value of limitId
     */
    public long getLimitId() {
        return limitId;
    }

    /**
     * Set the value of limitId
     *
     * @param limitId new value of limitId
     */
    public void setLimitId(long limitId) {
        this.limitId = limitId;
    }

    public String conditionCode;

    /**
     * Get the value of conditionCode
     *
     * @return the value of conditionCode
     */
    public String getConditionCode() {
        return conditionCode;
    }

    /**
     * Set the value of conditionCode
     *
     * @param conditionCode new value of conditionCode
     */
    public void setConditionCode(String conditionCode) {
        this.conditionCode = conditionCode;
    }

    public String conditionDescription;

    /**
     * Get the value of conditionDescription
     *
     * @return the value of conditionDescription
     */
    public String getConditionDescription() {
        return conditionDescription;
    }

    /**
     * Set the value of conditionDescription
     *
     * @param conditionDescription new value of conditionDescription
     */
    public void setConditionDescription(String conditionDescription) {
        this.conditionDescription = conditionDescription;
    }

    public String reasonCode;

    /**
     * Get the value of reasonCode
     *
     * @return the value of reasonCode
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * Set the value of reasonCode
     *
     * @param reasonCode new value of reasonCode
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    public String reasonDescription;

    /**
     * Get the value of reasonDescription
     *
     * @return the value of reasonDescription
     */
    public String getReasonDescription() {
        return reasonDescription;
    }

    /**
     * Set the value of reasonDescription
     *
     * @param reasonDescription new value of reasonDescription
     */
    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

}
