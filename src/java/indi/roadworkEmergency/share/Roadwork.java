/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.share;

/**
 *
 * @author pc1
 */
public class Roadwork {
    
    private long roadworkId = 0;
    private String name;
    private boolean isHighImpact = false;
    private String delay;
    private String description;
    private String advice;
    private String extraDetails;
    public Geometry geometry = null;
    public Limit limit = null;
    public Temporal temporal = null;
    
    
    public Roadwork()
    {
        geometry = new Geometry();
        limit = new Limit();
        temporal = new Temporal ();
        
    }
    /**
     * Get the value of roadworkId
     *
     * @return the value of roadworkId
     */
    public long getRoadworkId() {
        return roadworkId;
    }

    /**
     * Set the value of roadworkId
     *
     * @param roadworkId new value of roadworkId
     */
    public void setRoadworkId(long roadworkId) {
        this.roadworkId = roadworkId;
    }

    

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    

    /**
     * Get the value of isHighImpact
     *
     * @return the value of isHighImpact
     */
    public boolean isIsHighImpact() {
        return isHighImpact;
    }

    /**
     * Set the value of isHighImpact
     *
     * @param isHighImpact new value of isHighImpact
     */
    public void setIsHighImpact(boolean isHighImpact) {
        this.isHighImpact = isHighImpact;
    }

    

    /**
     * Get the value of description
     *
     * @return the value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Set the value of description
     *
     * @param description new value of description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    

    /**
     * Get the value of delay
     *
     * @return the value of delay
     */
    public String getDelay() {
        return delay;
    }

    /**
     * Set the value of delay
     *
     * @param delay new value of delay
     */
    public void setDelay(String delay) {
        this.delay = delay;
    }

    

    /**
     * Get the value of advice
     *
     * @return the value of advice
     */
    public String getAdvice() {
        return advice;
    }

    /**
     * Set the value of advice
     *
     * @param advice new value of advice
     */
    public void setAdvice(String advice) {
        this.advice = advice;
    }

    

    /**
     * Get the value of extraDetails
     *
     * @return the value of extraDetails
     */
    public String getExtraDetails() {
        return extraDetails;
    }

    /**
     * Set the value of extraDetails
     *
     * @param extraDetail new value of extraDetails
     */
    public void setExtraDetails(String extraDetail) {
        this.extraDetails = extraDetail;
    }
    

    /**
     * Get the value of geometry
     *
     * @return the value of geometry
     */
    public Geometry getGeometry() {
        return geometry;
    }

    /**
     * Set the value of geometry
     *
     * @param geometry new value of geometry
     */
    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }


    /**
     * Get the value of limit
     *
     * @return the value of limit
     */
    public Limit getLimit() {
        return limit;
    }

    /**
     * Set the value of limit
     *
     * @param limit new value of limit
     */
    public void setLimit(Limit limit) {
        this.limit = limit;
    }


    /**
     * Get the value of temporal
     *
     * @return the value of temporal
     */
    public Temporal getTemporal() {
        return temporal;
    }

    /**
     * Set the value of temporal
     *
     * @param temporal new value of temporal
     */
    public void setTemporal(Temporal temporal) {
        this.temporal = temporal;
    }

}
