/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.util;


import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 *
 * @author syunwei
 */
public class RENSRoadworkRequest {
    
    private SimpleDateFormat dateFormat; 
    private static final String MODIFIED_DATE_KEY = "If-Modified-Since";
    private static final String COOKIE_KEY = "Cookie";
    private static final String LAST_MODIFIED_KEY = "Last-Modified";
    private static final String DATE_FORMAT = "EEE, dd-MMM-yyyy hh:mm:ss z";
    private Properties prop;
    private Logger logger = Logger.getLogger(RENSRoadworkRequest.class);
    
    public RENSRoadworkRequest()
    {
        dateFormat = new SimpleDateFormat(DATE_FORMAT);
        
        try {
            loadProperty();
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RENSRoadworkRequest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void loadProperty()throws FileNotFoundException, IOException
    {
        if(prop == null)
        {
            prop = new Properties();
        }
        
        RENSFileControl fileControl = new RENSFileControl();
	FileInputStream input = new FileInputStream(fileControl.getFile("config.properties"));
        // load a properties file
	prop.load(input);
        input.close();
    }
    
    private String getProperty(String key) throws FileNotFoundException, IOException
    {
	// get the property value
	String value = prop.getProperty(key);
        return value;
    }
    
    private void setProperty(String key, String value) throws FileNotFoundException, IOException
    {
        if(key != null && value != null)
        {
            RENSFileControl fileControl = new RENSFileControl();
            FileOutputStream  output = new FileOutputStream(fileControl.getFile("config.properties"));
            // set the properties value
            prop.setProperty(key, value);
        
            // save properties to project root folder
            prop.store(output, null);
            output.close();
        }
	
        
    }
    
    private boolean dataSourceIsModified() throws MalformedURLException, IOException, ParseException
    {
        String url = "http://131940.qld.gov.au";
        URL urlObj = new URL(url);
        HttpURLConnection  con = (HttpURLConnection) urlObj.openConnection();
        String dateString = getProperty(MODIFIED_DATE_KEY);
        if(dateString != null)
        {
            // Set header field
            con.setRequestProperty("If-Modified-Since", dateString);
        }
        
        String originCookie = getProperty(COOKIE_KEY);
        
        if(originCookie != null)
        {
            con.setRequestProperty("Cookie", originCookie);
        }

        // Getting cookie
        con.connect();
        
        // Set cookie
        String cacheControlStr = con.getHeaderField("Cache-control");
        if(cacheControlStr.contains("no-cache=\"set-cookie\""))
        {
            String newCookie = con.getHeaderField("Set-Cookie");
            if(newCookie != null)
            {
                if(!newCookie.equals(originCookie))
                {
                    this.setProperty(COOKIE_KEY,newCookie);
                }
            }
        }
        else
        {
            String session = con.getHeaderField("Set-Cookie");
            if(session != null)
            {
                if(session.toLowerCase().contains("asp.net_sessionid"))
                {
                    String[] keyValues = session.split(";");
                    for(String keyValue : keyValues)
                    {
                        String[] keyAndValue = keyValue.split("=");
                        if(keyAndValue.length >= 2)
                        {
                            String sessionKey= keyAndValue[0];
                            if(sessionKey.toLowerCase().equals("asp.net_sessionid"))
                            {
                                String sessionValue = keyAndValue[1];
                                String newCookie = originCookie + ";" + sessionKey + "=" + sessionValue;
                                this.setProperty(COOKIE_KEY, newCookie);
                            }
                        }
                    }
                }
            }
            
        }
        
        // Get last modified date
        long lastmodified = con.getLastModified();
        if(lastmodified > 0)
        {
            Date lastModifiedDate = new Date(lastmodified);
            String lastModifiedDateStr = dateFormat.format(lastModifiedDate);
            
            String previousModified =  this.getProperty(LAST_MODIFIED_KEY);
            if(previousModified != null)
            {
                Date previousModifiedDate = dateFormat.parse(previousModified);
                if(lastModifiedDate.after(previousModifiedDate))
                {
                    // Data was updated
                    // Save last modified date
                    this.setProperty(LAST_MODIFIED_KEY, lastModifiedDateStr);
                    return true;
                }
            }
            else
            {
                this.setProperty(LAST_MODIFIED_KEY, lastModifiedDateStr);
                return true;
            }
            
        }
        
        return false;
    }
    
    public String request() throws Exception
    {
//        RENSFileControl fileControl = new RENSFileControl();
//        String mockStr = fileControl.getFileString("test/roadworksData.txt");
//        return mockStr;
        String url = "http://131940.qld.gov.au/api/json/v1/events/roadworks";
        URL urlObj = new URL(url);
        HttpURLConnection  con = (HttpURLConnection) urlObj.openConnection();

        // Set header fields
        con.setRequestProperty("Content-Type", "application/json");
        String res = con.getResponseMessage();
        //con.connect();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
        
    
}


