/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.util;

import com.sun.faces.facelets.util.Path;
import indi.roadworkEmergency.share.User;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.Vector;
import javafx.scene.shape.Shape;
import javapns.Push;
import javapns.communication.ProxyManager;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PayloadPerDevice;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.test.NotificationTest;
import javax.servlet.ServletContext;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;
import org.apache.log4j.*;
import org.json.JSONException;


/**
 *
 * @author pc1
 */

public class RENSPushControl {
    private static final String PushPwd = "Abc12345";

    public RENSPushControl() {
        try {
            BasicConfigurator.configure();
        } catch (Exception e) {
 }
    }
    
    public void pushToAll(String text) throws CommunicationException, KeystoreException, SQLException, JSONException
    {
        ClassLoader classLoader = getClass().getClassLoader();
	File cafile = new File(classLoader.getResource("/push/Certificates.p12").getFile());
        //InputStream is = context.getResourceAsStream("/WEB-INF/classes/push/Certificates.p12");
        RENSDBControl dbControl = new RENSDBControl();

        List<String> tokens = dbControl.selectUserTokens();
        PushNotificationPayload payload = PushNotificationPayload.complex();
        payload.addAlert("Alert : " + text); 
        payload.addSound("default");
        payload.addCustomDictionary("type", text);
        List<PushedNotification> notifications = Push.payload(payload, cafile, PushPwd, false, tokens);
    	   
            final  Logger logger = Logger.getLogger(RENSPushControl.class);
            for(PushedNotification notification : notifications)
           {
               logger.debug("push success:" + notification.isSuccessful() + " " +notification.getDevice().getToken());
               logger.debug("push response:" + notification.getResponse());
               Exception theProblem = notification.getException();
               if(theProblem != null)
               {
                   theProblem.printStackTrace();
               }
               
           }
    }
    
    public static void pushToDevices(List<String> deviceids, String content) throws CommunicationException, KeystoreException, SQLException
    {
        RENSFileControl fileControl =  new RENSFileControl(); 
        String c12 = fileControl.getFileString("/push/Cirtificates.p12");
        

        Push.alert(content, c12, PushPwd, false, deviceids);
    }
}
