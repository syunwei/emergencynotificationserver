/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.util;

import indi.roadworkEmergency.share.Limit;
import indi.roadworkEmergency.share.Roadwork;
import indi.roadworkEmergency.share.SDOGeometry;
import indi.roadworkEmergency.share.Temporal;
import indi.roadworkEmergency.share.User;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;
import oracle.jdbc.OraclePreparedStatement;
import oracle.spatial.geometry.JGeometry;
import org.apache.log4j.Logger;

/**
 *
 * @author pc1
 */
@Stateless
public class RENSDBControl {
    private Connection connection;
    final Logger logger = Logger.getLogger(RENSDBControl.class);
    
    public RENSDBControl()
    {
        // initialize oracle jdbc
        connection = getDBConnection();
        
        
    }
    
    private Limit createLimit(ResultSet rs) throws SQLException
    {
        Limit limit = new Limit();
        limit.setRoadworkId(rs.getLong("ROADWORKID"));
        limit.setConditionCode(rs.getString("CONDITIONCODE"));
        limit.setConditionDescription(rs.getString("CONDITIONDESCRIPTION"));
        limit.setReasonCode(rs.getString("REASONCODE"));
        limit.setReasonDescription(rs.getString("REASONDESCRIPTION"));
        return limit;
    }
    
    private Temporal createTemporal (ResultSet rs) throws SQLException
    {
        Temporal temporal = new Temporal();
        java.sql.Date startDate = rs.getDate("STARTDATE");
        java.sql.Date modifiedDate = rs.getDate("MODIFIEDDATE");
        java.sql.Date endDate = rs.getDate("ENDDATE");
        java.sql.Date review = rs.getDate("REVIEW");
        java.sql.Date lastReview = rs.getDate("LASTREVIEW");
        String nextUpdate = rs.getString("NEXTUPDATE");
        
        if(startDate != null)
        {
            temporal.setStartDate(new Date(startDate.getTime()));
        }
        
        if(modifiedDate != null)
        {
            temporal.setModifiedDate(new Date(modifiedDate.getTime()));
        }
        
        if(endDate != null)
        {
            temporal.setEndDate(new Date(endDate.getTime()));
        }
        
        if(review != null)
        {
            temporal.setReview(new Date(review.getTime()));
        }
        
        if(lastReview != null)
        {
            temporal.setLastReview(new Date(lastReview.getTime()));
        }
        
        temporal.setNextUpdate(nextUpdate);
        
        return temporal;
    }
    
    public List<Roadwork> selectAllRoadworks() throws SQLException, Exception
    {
        List<Roadwork> roadworks = new ArrayList<Roadwork>();
        String selectRoadwork = "SELECT * FROM ROADWORKS";
        OraclePreparedStatement selectRoadworkStmt = (OraclePreparedStatement) connection.prepareStatement(selectRoadwork);
        ResultSet rs = selectRoadworkStmt.executeQuery();
        while(rs.next())
        {
            Roadwork roadwork = createRoadwork(rs);
            
            roadworks.add(roadwork);
        }
        
        if(rs != null)
        {
            rs.close();
        }
        
        if(selectRoadworkStmt != null)
        {
            selectRoadworkStmt.close();
        }
        return roadworks;
    }
    
    public Roadwork selectRoadwork(long roadwork_id) throws SQLException, Exception
    {
        Roadwork roadwork = new Roadwork();
        String selectRoadwork = "SELECT * FROM ROADWORKS WHERE ROADWORKID = ?";
        OraclePreparedStatement selectRoadworkStmt = (OraclePreparedStatement) connection.prepareStatement(selectRoadwork);
        selectRoadworkStmt.setLong(1, roadwork_id);
        ResultSet rs = selectRoadworkStmt.executeQuery();
        while(rs.next())
        {
            roadwork.setRoadworkId(roadwork_id);
            roadwork.setName(rs.getString("NAME"));
            roadwork.setIsHighImpact(rs.getBoolean("ISHEIGHIMPACT"));
            roadwork.setDescription(rs.getString("DESCRIPTION"));
            roadwork.setDelay(rs.getString("DELAY"));
            roadwork.setAdvice(rs.getString("ADVICE"));
            roadwork.setExtraDetails(rs.getString("EXTRADETAILS"));
            
            // Select geometry
            String selectGeometry = "SELECT * FROM GEOMETRY WHERE ROADWORKID = ?";
            OraclePreparedStatement selectGeoStmt = (OraclePreparedStatement) connection.prepareStatement(selectGeometry);
            selectGeoStmt.setLong(1, roadwork_id);
            ResultSet geors = selectGeoStmt.executeQuery();
            while(geors.next())
            {
                roadwork.geometry.setRoadworkId(roadwork_id);
                roadwork.geometry.setType(geors.getString("TYPE"));
                roadwork.geometry.setAddress(geors.getString("ADDRESS"));
                byte[] image = geors.getBytes("LOCATION");
                JGeometry jgeo = JGeometry.load(image);
                roadwork.geometry.setLocation(jgeo);
            }
            if(geors != null)
            {
                geors.close();
            }
            
            if(selectGeoStmt!= null)
            {
                selectGeoStmt.close();
            }
            //Select limit
            String selectLimit = "SELECT * FROM LIMIT WHERE ROADWORKID = ?"; 
            OraclePreparedStatement selectLimitStmt = (OraclePreparedStatement) connection.prepareStatement(selectLimit);
            selectLimitStmt.setLong(1, roadwork_id);
            ResultSet limitrs = selectLimitStmt.executeQuery();
            while(limitrs.next())
            {
                Limit limit = createLimit(limitrs);
                roadwork.limit = limit;
            }
            
            if(limitrs != null)
            {
                limitrs.close();
            }
            if(selectLimitStmt != null)
            {
                selectLimitStmt.close();
            }
            
            // Select temporal
            String selectTemporal = "SELECT * FROM TEMPORAL WHERE ROADWORKID = ?";
            OraclePreparedStatement selectTemporalStmt = (OraclePreparedStatement) connection.prepareStatement(selectTemporal);
            selectTemporalStmt.setLong(1, roadwork_id);
            ResultSet temporalrs = selectTemporalStmt.executeQuery();
            while(temporalrs.next())
            {
                Temporal temporal = createTemporal(temporalrs);
                roadwork.temporal = temporal;
            }
            
            if(temporalrs != null)
            {
                temporalrs.close();
            }
            
            if(selectTemporalStmt != null)
            {
                selectTemporalStmt.close();
            }
        }
        
        if(rs != null)
        {
            rs.close();
        }
        
        if(selectRoadworkStmt != null)
        {
            selectRoadworkStmt.close();
        }
        return roadwork;
    }
    
    private Roadwork createRoadwork(ResultSet rs) throws SQLException, Exception
    {
        long roadwork_id = rs.getLong("ROADWORKID");
        Roadwork roadwork = new Roadwork();
        roadwork.setRoadworkId(roadwork_id);
        roadwork.setName(rs.getString("NAME"));
        roadwork.setIsHighImpact(rs.getBoolean("ISHEIGHIMPACT"));
        roadwork.setDescription(rs.getString("DESCRIPTION"));
        roadwork.setDelay(rs.getString("DELAY"));
        roadwork.setAdvice(rs.getString("ADVICE"));
        roadwork.setExtraDetails(rs.getString("EXTRADETAILS"));
            
            // Select geometry
            String selectGeometry = "SELECT * FROM GEOMETRY WHERE ROADWORKID = ?";
            OraclePreparedStatement selectGeoStmt = (OraclePreparedStatement) connection.prepareStatement(selectGeometry);
            selectGeoStmt.setLong(1, roadwork_id);
            ResultSet geors = selectGeoStmt.executeQuery();
            while(geors.next())
            {
                roadwork.geometry.setRoadworkId(roadwork_id);
                roadwork.geometry.setType(geors.getString("TYPE"));
                roadwork.geometry.setAddress(geors.getString("ADDRESS"));
                byte[] image = geors.getBytes("LOCATION");
                JGeometry jgeo = JGeometry.load(image);
                roadwork.geometry.setLocation(jgeo);
            }
            if(geors != null)
            {
                geors.close();
            }
            
            if(selectGeoStmt!= null)
            {
                selectGeoStmt.close();
            }
            //Select limit
            String selectLimit = "SELECT * FROM LIMIT WHERE ROADWORKID = ?"; 
            OraclePreparedStatement selectLimitStmt = (OraclePreparedStatement) connection.prepareStatement(selectLimit);
            selectLimitStmt.setLong(1, roadwork_id);
            ResultSet limitrs = selectLimitStmt.executeQuery();
            while(limitrs.next())
            {
                Limit limit = createLimit(limitrs);
                roadwork.limit = limit;
            }
            
            if(limitrs != null)
            {
                limitrs.close();
            }
            if(selectLimitStmt != null)
            {
                selectLimitStmt.close();
            }
            
            // Select temporal
            String selectTemporal = "SELECT * FROM TEMPORAL WHERE ROADWORKID = ?";
            OraclePreparedStatement selectTemporalStmt = (OraclePreparedStatement) connection.prepareStatement(selectTemporal);
            selectTemporalStmt.setLong(1, roadwork_id);
            ResultSet temporalrs = selectTemporalStmt.executeQuery();
            while(temporalrs.next())
            {
                Temporal temporal = createTemporal(temporalrs);
                roadwork.temporal = temporal;
            }
            
            if(temporalrs != null)
            {
                temporalrs.close();
            }
            
            if(selectTemporalStmt != null)
            {
                selectTemporalStmt.close();
            }
            
            return roadwork;
    }
    public List<Roadwork> selectNearestRoadworks(double longitude, double latitude, double radius) throws SQLException, Exception
    {
        List<Roadwork> roadworks = new ArrayList<Roadwork>();
        
        String selectRoadwork = "SELECT * FROM ROADWORKS r, GEOMETRY g WHERE r.ROADWORKID = g.ROADWORKID AND SDO_WITHIN_DISTANCE(g.LOCATION, SDO_GEOMETRY(3001, 8307, SDO_POINT_TYPE(?,?, NULL), NULL, NULL), ? ) = 'TRUE'";
        OraclePreparedStatement selectRoadworkStmt = (OraclePreparedStatement) connection.prepareStatement(selectRoadwork);
        selectRoadworkStmt.setDouble(1, longitude);
        selectRoadworkStmt.setDouble(2, latitude);
        selectRoadworkStmt.setString(3, String.format("DISTANCE=%f UNIT=KM", radius));//String.format("DISTANCE=%e UNIT=KM", radius)
        
        
        ResultSet rs = selectRoadworkStmt.executeQuery();
        while(rs.next())
        {
            Roadwork roadwork = createRoadwork(rs);
            roadworks.add(roadwork);
        }
        
        if(rs != null)
        {
            rs.close();
        }
        
        if(selectRoadworkStmt != null)
        {
            selectRoadworkStmt.close();
        }
        
        return roadworks;
    }
    public void deleteRoadwork(long roadworkId) throws SQLException
    {
        // Delete limit data row
        String deleteLimitSql = "DELETE FROM LIMIT WHERE ROADWORKID = ?";
        OraclePreparedStatement limitpstmt = (OraclePreparedStatement) connection.prepareStatement(deleteLimitSql);
        limitpstmt.setLong(1, roadworkId);
        limitpstmt.execute();
        limitpstmt.close();
        //Delete geometry data row
        String deleteGeometrySql = "DELETE FROM GEOMETRY WHERE ROADWORKID = ?";
        OraclePreparedStatement geometrypstmt = (OraclePreparedStatement) connection.prepareStatement(deleteGeometrySql);
        geometrypstmt.setLong(1, roadworkId);
        geometrypstmt.execute();
        geometrypstmt.close();
        
        //Delte temporal data row
        String deleteTemporalSql = "DELETE FROM TEMPORAL WHERE ROADWORKID = ?";
        OraclePreparedStatement temporalpstmt = (OraclePreparedStatement) connection.prepareStatement(deleteTemporalSql);
        temporalpstmt.setLong(1, roadworkId);
        temporalpstmt.execute();
        temporalpstmt.close();
        
        //Delete roadwork data row
        String deleteRoadworkSql = "DELETE FROM ROADWORKS WHERE ROADWORKID = ?";
        OraclePreparedStatement roadworkpstmt = (OraclePreparedStatement) connection.prepareStatement(deleteRoadworkSql);
        roadworkpstmt.setLong(1, roadworkId);
        roadworkpstmt.execute();
        roadworkpstmt.close();
    }
    
    public void insertRoadwork(Roadwork roadwork)throws SQLException, Exception
    {
        OraclePreparedStatement pstmt = null;
        // INSERT INTO ROADWORKS (Name, IsHeighImpact, Description) VALUES ('Roadwork5','1','This is roadwork5 description.') ;
        if(roadwork.getRoadworkId()>0)
        {
            String roadworkSql = "BEGIN "
                + "INSERT INTO ROADWORKS(RoadworkId, Name, IsHeighImpact, Description, Delay, Advice, ExtraDetails) VALUES (?,?,?,?,?,?,?);"
                + "EXCEPTION WHEN DUP_VAL_ON_INDEX THEN "
                + "UPDATE ROADWORKS SET Name = ?, IsHeighImpact = ?, Description = ?, Delay = ?, Advice = ?, ExtraDetails = ? WHERE ROADWORKID = ?;"
                + "END;";
            pstmt = (OraclePreparedStatement) connection.prepareStatement(roadworkSql);
            pstmt.setLong(1, roadwork.getRoadworkId());
            pstmt.setString(2, roadwork.getName());
            pstmt.setBoolean(3, roadwork.isIsHighImpact());
            pstmt.setString(4, roadwork.getDescription());
            pstmt.setString(5, roadwork.getDelay());
            pstmt.setString(6, roadwork.getAdvice());
            pstmt.setString(7, roadwork.getExtraDetails());
            pstmt.setString(8, roadwork.getName());
            pstmt.setBoolean(9, roadwork.isIsHighImpact());
            pstmt.setString(10, roadwork.getDescription());
            pstmt.setString(11, roadwork.getDelay());
            pstmt.setString(12, roadwork.getAdvice());
            pstmt.setString(13, roadwork.getExtraDetails());
            pstmt.setLong(14, roadwork.getRoadworkId());
            pstmt.execute();
        }
        else
        {
            String roadworkSql = "INSERT INTO ROADWORKS(Name, IsHeighImpact, Description, Delay, Advice, ExtraDetails) VALUES (?,?,?,?,?,?) RETURNING roadworkid INTO ?";
                    //+ "EXCEPTION WHEN DUP_VAL_ON_INDEX THEN ";
            pstmt = (OraclePreparedStatement) connection.prepareStatement(roadworkSql);
            pstmt.setString(1, roadwork.getName());
            pstmt.setBoolean(2, roadwork.isIsHighImpact());
            pstmt.setString(3, roadwork.getDescription());
            pstmt.setString(4, roadwork.getDelay());
            pstmt.setString(5, roadwork.getAdvice());
            pstmt.setString(6, roadwork.getExtraDetails());
            pstmt.registerReturnParameter(7, Types.BIGINT);
            pstmt.execute();
            try (ResultSet rs = pstmt.getReturnResultSet()) {
                while(rs.next())
                {
                    long roadworkId = rs.getLong(1);
                    roadwork.setRoadworkId(roadworkId);
                }
            }
        }
        
        
        
        OraclePreparedStatement geometryPstmt = null;
        String geometrySql = "BEGIN"
                + " INSERT INTO GEOMETRY (RoadworkId, Type, Address, Location) VALUES (?, ?, ?, ?);"
                + " EXCEPTION WHEN DUP_VAL_ON_INDEX THEN"
                + " UPDATE GEOMETRY SET Type = ?, Address = ?, Location = ? WHERE ROADWORKID = ?;"
                + " END;";
        
        geometryPstmt = (OraclePreparedStatement) connection.prepareStatement(geometrySql);
        geometryPstmt.setLong(1, roadwork.getRoadworkId());
        geometryPstmt.setString(2, roadwork.getGeometry().getType());
        geometryPstmt.setString(3, roadwork.getGeometry().getAddress());
        geometryPstmt.setObject(4, JGeometry.storeJS(connection, roadwork.geometry.location)); //2 dimentions, 2 linear referencing.
        geometryPstmt.setString(5, roadwork.getGeometry().getType());
        geometryPstmt.setString(6, roadwork.getGeometry().getAddress());
        geometryPstmt.setObject(7, JGeometry.storeJS(connection, roadwork.geometry.location)); //2 dimentions, 2 linear referencing.
        geometryPstmt.setLong(8, roadwork.getRoadworkId());
        geometryPstmt.execute();
        
        OraclePreparedStatement limitPstmt = null;
        String limitSql = "BEGIN"
                + " INSERT INTO LIMIT (ROADWORKID, CONDITIONCODE, CONDITIONDESCRIPTION, REASONCODE, REASONDESCRIPTION) VALUES (?, ?, ?, ?, ?);"
                + " EXCEPTION WHEN DUP_VAL_ON_INDEX THEN"
                + " UPDATE LIMIT SET CONDITIONCODE = ?, CONDITIONDESCRIPTION = ?, REASONCODE = ?, REASONDESCRIPTION = ? WHERE ROADWORKID = ?;"
                + " END;";
        limitPstmt = (OraclePreparedStatement) connection.prepareStatement(limitSql);
        limitPstmt.setLong(1, roadwork.getRoadworkId());
        limitPstmt.setString(2, roadwork.limit.getConditionCode());
        limitPstmt.setString(3, roadwork.limit.getConditionDescription());
        limitPstmt.setString(4, roadwork.limit.getReasonCode());
        limitPstmt.setString(5, roadwork.limit.getReasonDescription());
        limitPstmt.setString(6, roadwork.limit.getConditionCode());
        limitPstmt.setString(7, roadwork.limit.getConditionDescription());
        limitPstmt.setString(8, roadwork.limit.getReasonCode());
        limitPstmt.setString(9, roadwork.limit.getReasonDescription());
        limitPstmt.setLong(10, roadwork.getRoadworkId());
        limitPstmt.execute();
        
        
        String temporalSql = "BEGIN"
                + " INSERT INTO TEMPORAL (ROADWORKID, STARTDATE, MODIFIEDDATE, ENDDATE, REVIEW, LASTREVIEW, NEXTUPDATE) VALUES (?, ?, ?, ?, ?, ?, ?);"
                + " EXCEPTION WHEN DUP_VAL_ON_INDEX THEN"
                + " UPDATE TEMPORAL SET STARTDATE = ?, MODIFIEDDATE = ?, ENDDATE = ?, REVIEW = ?, LASTREVIEW = ?, NEXTUPDATE = ? WHERE ROADWORKID = ?;"
                + " END;";
        OraclePreparedStatement temporalPstmt = (OraclePreparedStatement)connection.prepareStatement(temporalSql);
        
        Date startDate = null;
        Date modifiedDate = null;
        Date endDate = null;
        Date review = null;
        Date lastReview = null;
        
        if(roadwork.temporal.getStartDate() != null)
        {
            startDate = new java.sql.Date(roadwork.temporal.getStartDate().getTime());
        }
        
        if(roadwork.temporal.getModifiedDate() != null)
        {
            modifiedDate = new java.sql.Date(roadwork.temporal.getModifiedDate().getTime());
        }
        
        if(roadwork.temporal.getEndDate() != null)
        {
            endDate = new java.sql.Date(roadwork.temporal.getEndDate().getTime());
        }
        
        if(roadwork.temporal.getReview()!= null)
        {
            review = new java.sql.Date(roadwork.temporal.getReview().getTime());
        }
        
        if(roadwork.temporal.getLastReview() != null)
        {
            lastReview = new java.sql.Date(roadwork.temporal.getLastReview().getTime());
        }
        temporalPstmt.setLong(1, roadwork.getRoadworkId());
 
        temporalPstmt.setDate(2,  startDate);
        temporalPstmt.setDate(3, modifiedDate);
        temporalPstmt.setDate(4, endDate);
        temporalPstmt.setDate(5, review);
        temporalPstmt.setDate(6, lastReview);
        temporalPstmt.setString(7, roadwork.temporal.getNextUpdate());
        temporalPstmt.setDate(8,  startDate);
        temporalPstmt.setDate(9, modifiedDate);
        temporalPstmt.setDate(10, endDate);
        temporalPstmt.setDate(11, review);
        temporalPstmt.setDate(12, lastReview);
        temporalPstmt.setString(13, roadwork.temporal.getNextUpdate());
        temporalPstmt.setLong(14, roadwork.getRoadworkId());
        temporalPstmt.execute();
        
        if(temporalPstmt != null)
        {
            temporalPstmt.close();
        }
        
        if(limitPstmt != null)
        {
            limitPstmt.close();
        }
        
        if(geometryPstmt != null)
        {
            geometryPstmt.close();
        }
        
        if(pstmt != null)
        {
            pstmt.close();
        }
    } 
    
    public List selectUsers() throws SQLException
    {
        List <User> users = new ArrayList<User>() ;
        String selectUsers = "SELECT USERS.userid, USERS.username, DEVICES.deviceid FROM USERS JOIN DEVICES ON USERS.userid = DEVICES.userid" +
                             " WHERE DEVICES.PLATFORMID = 1";
        OraclePreparedStatement selectUsersStmt = (OraclePreparedStatement) connection.prepareStatement(selectUsers);
        ResultSet rs = selectUsersStmt.executeQuery();
        while (rs.next())
        {
            User user = new User();
            user.setUserId(rs.getLong("USERID"));
            user.setUserName(rs.getString("USERNAME"));
            user.setDeviceId(rs.getString("DEVICEID"));
            users.add(user);
        }
        
        if (rs != null)
        {
            rs.close();
        }
        
        if(selectUsersStmt != null)
        {
            selectUsersStmt.close();
        }
        
        return users;
    }
    
    public List selectUserTokens() throws SQLException
    {
        List <String> tokens = new ArrayList<String>() ;
        String selectTokens = "SELECT DEVICES.token FROM USERS JOIN DEVICES ON USERS.userid = DEVICES.userid" +
                             " WHERE DEVICES.PLATFORMID = 1";
        OraclePreparedStatement selectUsersTokenStmt = (OraclePreparedStatement) connection.prepareStatement(selectTokens);
        ResultSet rs = selectUsersTokenStmt.executeQuery();
        while (rs.next())
        {
            tokens.add(rs.getString("TOKEN"));
        }
        
        if (rs != null)
        {
            rs.close();
        }
        
        if(selectUsersTokenStmt != null)
        {
            selectUsersTokenStmt.close();
        }
        return tokens;
    }
    
    public String getUserToken(String deviceID) throws SQLException
    {
        String selectToken = "SELECT DEVICES.token FROM USERS JOIN DEVICES ON USERS.userid = DEVICES.userid" +
                             " WHERE DEVICES.PLATFORMID = 1 AND DEVICEID = ?";
        OraclePreparedStatement selectUsersStmt = (OraclePreparedStatement) connection.prepareStatement(selectToken);
        selectUsersStmt.setString(1, deviceID);
        ResultSet rs = selectUsersStmt.executeQuery();
        String token = "";
        while (rs.next())
        {
            token = rs.getString("TOKEN");
        }
        
        if(rs != null)
        {
            rs.close();
        }
        
        if (selectUsersStmt != null)
        {
            selectUsersStmt.close();
        }
        return token;
    }
    
    public boolean insertUserToken(User user) throws SQLException
    {
        OraclePreparedStatement tokenPstmt = null;
        String insertTokenSql = "BEGIN INSERT INTO DEVICES (USERID, DEVICEID, PLATFORMID, TOKEN) VALUES (?, ?, ?, ?);" +
                                " EXCEPTION WHEN DUP_VAL_ON_INDEX THEN" +
                                " UPDATE DEVICES SET TOKEN = ? WHERE USERID = ? AND DEVICEID = ?;" +
                                " END;";
        
        tokenPstmt = (OraclePreparedStatement) connection.prepareStatement(insertTokenSql);
        tokenPstmt.setLong(1, user.getUserId());
        tokenPstmt.setString(2, user.getDeviceId());
        tokenPstmt.setInt(3, 1);
        tokenPstmt.setString(4, user.getToken());
        tokenPstmt.setString(5, user.getToken());
        tokenPstmt.setLong(6, user.getUserId());
        tokenPstmt.setString(7, user.getDeviceId());
        
        if(tokenPstmt != null)
        {
            tokenPstmt.close();
        }
        
        return tokenPstmt.execute();
    }
    
    private Connection getDBConnection()
    {
        if (connection == null)
        {
            try
            {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            }
            catch(Exception ex)
            {
                System.out.println("RENDBControl: Can't find Oracle JDBC driver.");
                System.out.println(ex);
            }
        
            try
            {
                String dbURL = "jdbc:oracle:thin:@localhost:1521:orcl";
                String username = "c##admin";
                String pwd = "admin";
            
                connection = DriverManager.getConnection(dbURL, username, pwd);
            }
            catch(Exception ex)
            {
                System.out.println("RENSDBControl: Connection failed.");
                System.out.println(ex.getMessage());
            }
        }
        return connection;
    }
    
    public void updateUserLocation(double longitude, double latitude, long userid) throws SQLException, Exception
    {
        JGeometry jgeo = new JGeometry(longitude, latitude, 8307);
        String userlocationSql = "UPDATE USERS SET LOCATION = ? WHERE USERID = ?";
        OraclePreparedStatement userlocationPstmt = (OraclePreparedStatement) connection.prepareStatement(userlocationSql);
        userlocationPstmt.setObject(1,JGeometry.store(jgeo,connection));
        userlocationPstmt.setObject(2,userid);
        userlocationPstmt.executeUpdate();
        userlocationPstmt.close();
    }
    
    public void closeConnection() throws SQLException
    {
        connection.close();
    }
}
