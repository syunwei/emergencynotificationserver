/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package indi.roadworkEmergency.util;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;


/**
 *
 * @author pc1
 */

public class RENSFileControl {
    
    public String getFileString(String fileName) {
 
	StringBuilder result = new StringBuilder("");
 
	//Get file from resources folder
	ClassLoader classLoader = getClass().getClassLoader();
	File file = new File(classLoader.getResource(fileName).getFile());
 
	try (Scanner scanner = new Scanner(file)) {
 
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			result.append(line).append("\n");
		}
 
		scanner.close();
 
	} catch (IOException e) {
		e.printStackTrace();
	}
 
	return result.toString();
 
  }
    
    public File getFile(String fileName) {
 
	StringBuilder result = new StringBuilder("");
 
	//Get file from resources folder
	ClassLoader classLoader = getClass().getClassLoader();
	File file = new File(classLoader.getResource(fileName).getFile());
 
	
 
	return file;
  }
    
    public String getFilePath(String fileName) {
 
	//Get file from resources folder
	ClassLoader classLoader = getClass().getClassLoader();
	String path = classLoader.getResource(fileName).getPath();
 
	return path;
 
  }
}
