# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is the back-end code for Roadwork Emergency System.
* Version 1.0


### How do I get set up? ###

* Summary of set up
    * GlassFish Server
    * Push notification environment(.p12 file is needed)
    * Oracle Database

* Dependencies
    * Log4j
    * JavaPNS

* Database configuration
    * Download and install Oracle Database
    * Create an user for the database
    * Create tables
    * Set up spacial data tables